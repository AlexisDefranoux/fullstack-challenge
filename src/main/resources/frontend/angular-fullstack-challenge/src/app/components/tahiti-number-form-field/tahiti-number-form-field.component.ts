import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-tahiti-number-form-field',
  templateUrl: './tahiti-number-form-field.component.html',
  styleUrls: ['./tahiti-number-form-field.component.scss']
})
export class TahitiNumberFormFieldComponent implements OnInit {

  @Input() formGroup!: FormGroup

  ngOnInit(): void {
    this.formGroup.addControl('tahitiNumber', new FormControl('', [Validators.required, Validators.pattern('^T[a-zA-Z0-9]{1}[0-9]{4}$')]));
  }

}
