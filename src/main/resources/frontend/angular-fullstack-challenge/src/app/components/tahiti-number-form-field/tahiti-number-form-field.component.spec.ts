import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TahitiNumberFormFieldComponent} from './tahiti-number-form-field.component';

describe('TahitiNumberFormFieldComponent', () => {
  let component: TahitiNumberFormFieldComponent;
  let fixture: ComponentFixture<TahitiNumberFormFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TahitiNumberFormFieldComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TahitiNumberFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
