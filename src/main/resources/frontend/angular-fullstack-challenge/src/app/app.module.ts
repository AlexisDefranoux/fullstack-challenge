import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "./material.module";
import {HttpClientModule} from "@angular/common/http";
import {ContactFormComponent} from "./components/contact-form/contact-form.component";
import {EmailFormFieldComponent} from "./components/email-form-field/email-form-field.component";
import {TahitiNumberFormFieldComponent} from "./components/tahiti-number-form-field/tahiti-number-form-field.component";
import {TaxTypeFormFieldComponent} from "./components/tax-type-form-field/tax-type-form-field.component";

@NgModule({
  declarations: [
    AppComponent,
    ContactFormComponent,
    EmailFormFieldComponent,
    TahitiNumberFormFieldComponent,
    TaxTypeFormFieldComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,

  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
