package com.alexis.defranoux.fullstack.challenge.backend.controllers;

import com.alexis.defranoux.fullstack.challenge.backend.entities.ContactForm;
import com.alexis.defranoux.fullstack.challenge.backend.exceptions.ContactFormException;
import com.alexis.defranoux.fullstack.challenge.backend.repositories.ContactFormRepository;
import com.alexis.defranoux.fullstack.challenge.backend.services.ContactFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;

@RestController()
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/contact-forms")
public class ContactFormController {

    private final ContactFormRepository contactFormRepository;
    private final ContactFormService contactFormService;

    @Autowired
    public ContactFormController(
            ContactFormRepository contactFormRepository,
            ContactFormService contactFormService
    ) {
        this.contactFormService = contactFormService;
        this.contactFormRepository = contactFormRepository;
    }

    @PostMapping()
    public ResponseEntity<ContactForm> addContactForm(@RequestBody ContactForm contactForm) throws MessagingException {
        try {
            contactFormService.sendContactForm(contactForm);
            return ResponseEntity.status(HttpStatus.OK).body(contactFormRepository.save(contactForm));
        } catch (ContactFormException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(contactForm);
        }
    }

    @GetMapping()
    public List<ContactForm> getContactFormService() {
        return (List<ContactForm>) contactFormRepository.findAll();
    }

    @GetMapping("/{id}")
    public ContactForm getContactFormById(@PathVariable(value = "id") Long contactFormId) {
        return contactFormRepository.findById(contactFormId).get();
    }

    @DeleteMapping()
    public void deleteContactForms() {
        contactFormRepository.findAll().forEach(contactFormRepository::delete);
    }

    @DeleteMapping("/{id}")
    public void deleteContactForm(@PathVariable(value = "id") Long contactFormId) {
        contactFormRepository.deleteById(contactFormId);
    }

    @PutMapping("/{id}")
    public Optional<ContactForm> replaceContactForm(@PathVariable(value = "id") long contactFormId, @RequestBody ContactForm newContactForm) {
        return contactFormRepository.findById(contactFormId).map(oldContactForm -> {
            oldContactForm.setEmail(newContactForm.getEmail());
            oldContactForm.setQuestionType(newContactForm.getQuestionType());
            oldContactForm.setTahitiNumber(newContactForm.getTahitiNumber());
            oldContactForm.setTaxType(newContactForm.getTaxType());
            oldContactForm.setFile(newContactForm.getFile());
            return contactFormRepository.save(oldContactForm);
        });
    }

}
