import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-email-form-field',
  templateUrl: './email-form-field.component.html',
  styleUrls: ['./email-form-field.component.scss']
})
export class EmailFormFieldComponent implements OnInit {

  @Input() formGroup!: FormGroup

  ngOnInit(): void {
    this.formGroup.addControl('email', new FormControl('', [Validators.required, Validators.email]));
  }

}
