package com.alexis.defranoux.fullstack.challenge.backend.exceptions;

public class TahitiNumberContactFormException extends ContactFormException {

    public TahitiNumberContactFormException(String tahitiNumber) {
        super("Invalid tahiti number format: " + tahitiNumber);
    }
}
