package com.alexis.defranoux.fullstack.challenge.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullstackChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FullstackChallengeApplication.class, args);
	}

}
