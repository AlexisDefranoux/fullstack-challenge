import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TaxTypeFormFieldComponent} from './tax-type-form-field.component';

describe('TaxTypeFormFieldComponent', () => {
  let component: TaxTypeFormFieldComponent;
  let fixture: ComponentFixture<TaxTypeFormFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaxTypeFormFieldComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxTypeFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
