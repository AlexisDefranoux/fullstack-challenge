package com.alexis.defranoux.fullstack.challenge.backend.services;

import com.alexis.defranoux.fullstack.challenge.backend.entities.ContactForm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    private final JavaMailSender mailSender;
    @Value("${app.default-email-sender}")
    private String defaultEmailSender;

    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendEmail(ContactForm contactForm) throws MessagingException {
        MimeMessage message = this.mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom(defaultEmailSender);
        helper.setTo(contactForm.getQuestionType().getEmail());
        helper.setText(createEmailText(contactForm), true);
        helper.setSubject(StringUtils.capitalize(contactForm.getQuestionType().getLabel()));

        if (contactForm.getFile() != null) {
            helper.addAttachment("Pièce jointe", contactForm.getFile());
        }

        mailSender.send(message);
    }

    private String createEmailText(ContactForm contactForm) {
        StringBuilder emailTextBuilder = new StringBuilder();
        emailTextBuilder.append("<p>Bonjour,</p><p>Voici les informations pour une de demande de ").append(contactForm.getQuestionType().getLabel()).append(" : <ul>");
        if (contactForm.getTahitiNumber() != null) {
            emailTextBuilder.append("<li>Numéro Tahiti : ").append(contactForm.getTahitiNumber()).append("</li>");
        }
        if (contactForm.getEmail() != null) {
            emailTextBuilder.append("<li>Email : ").append(contactForm.getEmail()).append("</li>");
        }
        if (contactForm.getTaxType() != null) {
            emailTextBuilder.append("<li>Type d'impôt : ").append(contactForm.getTaxType().getLabel()).append("</li>");
        }
        if (contactForm.getComment() != null) {
            emailTextBuilder.append("<li>Remarques : ").append(contactForm.getComment()).append("</li>");
        }
        emailTextBuilder.append("</ul></p><p>Cordialement,</p><p>Système <i>fullstack challenge</i></p>");
        return emailTextBuilder.toString();
    }

}