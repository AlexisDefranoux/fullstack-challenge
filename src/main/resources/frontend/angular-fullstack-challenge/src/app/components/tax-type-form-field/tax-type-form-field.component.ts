import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TAX_TYPES} from "../../constants/tax-type.constant";

@Component({
  selector: 'app-tax-type-form-field',
  templateUrl: './tax-type-form-field.component.html',
  styleUrls: ['./tax-type-form-field.component.scss']
})
export class TaxTypeFormFieldComponent implements OnInit {

  @Input() formGroup!: FormGroup
  taxTypes = TAX_TYPES;

  ngOnInit(): void {
    this.formGroup.addControl('taxType', new FormControl('', [Validators.required]));
  }

}
