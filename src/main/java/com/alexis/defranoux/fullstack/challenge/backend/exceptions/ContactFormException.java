package com.alexis.defranoux.fullstack.challenge.backend.exceptions;

public class ContactFormException extends Exception {

    public ContactFormException(String message) {
        super(message);
    }
}
