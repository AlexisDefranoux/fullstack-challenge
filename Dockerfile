FROM openjdk:11.0.11-jre-slim-buster

WORKDIR /app

COPY target/fullstack-challenge-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "fullstack-challenge-0.0.1-SNAPSHOT.jar"]
