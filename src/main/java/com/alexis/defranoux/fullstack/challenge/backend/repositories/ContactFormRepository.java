package com.alexis.defranoux.fullstack.challenge.backend.repositories;

import com.alexis.defranoux.fullstack.challenge.backend.entities.ContactForm;
import org.springframework.data.repository.CrudRepository;

public interface ContactFormRepository extends CrudRepository<ContactForm, Long> {
}
