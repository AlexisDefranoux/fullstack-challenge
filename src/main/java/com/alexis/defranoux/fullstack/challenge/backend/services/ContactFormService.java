package com.alexis.defranoux.fullstack.challenge.backend.services;

import com.alexis.defranoux.fullstack.challenge.backend.controllers.EmailController;
import com.alexis.defranoux.fullstack.challenge.backend.entities.ContactForm;
import com.alexis.defranoux.fullstack.challenge.backend.exceptions.ContactFormException;
import com.alexis.defranoux.fullstack.challenge.backend.exceptions.EmailContactFormException;
import com.alexis.defranoux.fullstack.challenge.backend.exceptions.TahitiNumberContactFormException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.util.regex.Pattern;

@Service
public class ContactFormService {

    private final EmailController emailController;

    private ContactFormService(EmailController emailController) {
        this.emailController = emailController;
    }

    private void emailValidation(String email) throws EmailContactFormException {
        String emailRegex = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
        if (!Pattern.compile(emailRegex).matcher(email).matches()) {
            throw new EmailContactFormException(email);
        }
    }

    private void tahitiNumberValidation(String tahitiNumber) throws TahitiNumberContactFormException {
        String emailRegex = "^T[a-zA-Z0-9]{1}[0-9]{4}$";
        if (!Pattern.compile(emailRegex).matcher(tahitiNumber).matches()) {
            throw new TahitiNumberContactFormException(tahitiNumber);
        }
    }

    //TODO file security check
    private void fileValidation(File file) {
    }

    private void formValidation(ContactForm contactForm) throws ContactFormException {
        switch (contactForm.getQuestionType()) {
            case DICP_RECEIPT:
                tahitiNumberValidation(contactForm.getTahitiNumber());
                break;
            case LITIGATION:
                emailValidation(contactForm.getEmail());
                tahitiNumberValidation(contactForm.getTahitiNumber());
                break;
            case STATEMENT:
                emailValidation(contactForm.getEmail());
                tahitiNumberValidation(contactForm.getTahitiNumber());
                fileValidation(contactForm.getFile());
                break;
            default:
        }
    }

    public void sendContactForm(ContactForm contactForm) throws MessagingException, ContactFormException {
        formValidation(contactForm);
        emailController.send(contactForm);
    }


}
