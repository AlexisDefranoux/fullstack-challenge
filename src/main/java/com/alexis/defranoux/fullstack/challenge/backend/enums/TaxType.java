package com.alexis.defranoux.fullstack.challenge.backend.enums;

public enum TaxType {

    IS("Impôt sur le salaire"),
    TVA("Taxe sur la valeur ajoutée"),
    CSG("Contribution sociale généralisée");

    private final String label;

    TaxType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
