# Fullstack challenge

## Frontend environment

The frontend application is generated with:

- [Angular CLI](https://github.com/angular/angular-cli) version `12.0.0`
- [Npm](https://www.npmjs.com/) version `7.13.0`
- [Node.js](https://nodejs.org/) version `14.17.0`

## Backend environment

The backend server is generated with:

- Maven version `3.8.1`
- Java version `11`
- [Spring Boot](https://spring.io/projects/spring-boot) version `2.4.5`

## Docker environment

You need to install [Docker](https://www.docker.com/).

## How to run ?

1. Open a terminal in `src/main/resources/frontend/angular-fullstack-challenge` and run:

```sh 
npm install
```

2. Then, open a terminal in the root project and run:

```sh 
mvn clean package
```

3. Next, run the command:

```sh 
docker pull inbucket/inbucket
```

4. At the end, run the command:

```sh 
docker-compose up
```

5. Open a browser at [http://localhost:9000/monitor](http://localhost:9000/monitor) for Inbucket interface
   and [http://localhost:8080/](http://localhost:8080/) for the fullstack challenge application

## Run frontend separately

Open a terminal in the folder `fullstack-challenge/src/main/resources/frontend` and run:

```sh 
npm install
```

```
ng serve
```

The angular application is running on [http://localhost:4200/](http://localhost:4200/).

## Run backend separately

Modify the value `spring.mail.host` to `localhost` in `fullstack-challenge/src/main/resources/application.properties`.

Then, open a terminal in the root project and run:

```sh 
mvnw spring-boot:run
```

The Spring server is running on [http://localhost:8080/](http://localhost:8080/).

_Note: don't forget to update the value `spring.mail.host` to `inbucket` before `mvn clean package`
and `docker-compose up`._

## Run Inbucket separately

To receive emails delivered by the backend, you can use [Inbucket](https://www.inbucket.org/) as a fake STMP server.

Open a terminal and run:

```sh 
docker run -d --name inbucket -p 9000:9000 -p 2500:2500 -p 1100:1100 inbucket/inbucket
```

The Inbucket interface is running on [http://localhost:9000/monitor](http://localhost:9000/monitor).