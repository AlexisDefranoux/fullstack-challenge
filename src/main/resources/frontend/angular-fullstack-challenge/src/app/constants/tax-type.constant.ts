export const TAX_TYPES = [
  {value: 'IS', label: 'Impôt sur le salaire'},
  {value: 'TVA', label: 'Taxe sur la valeur ajoutée'},
  {value: 'CSG', label: 'Contribution sociale généralisée'}
];
