export const QUESTION_TYPES = [
  {value: 'DICP_RECEIPT', label: 'Justificatif DICP'},
  {value: 'LITIGATION', label: 'Contentieux'},
  {value: 'STATEMENT', label: 'Déclaration'}
];
