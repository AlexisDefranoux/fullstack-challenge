package com.alexis.defranoux.fullstack.challenge.backend.enums;

public enum QuestionType {

    DICP_RECEIPT("justificatif DICP", "justificatif-dicp@fullstack-challenge.com"),
    LITIGATION("contentieux", "contentieux@fullstack-challenge.com"),
    STATEMENT("déclaration", "declaration@fullstack-challenge.com");

    private final String label;
    private final String email;

    QuestionType(String label, String email) {
        this.label = label;
        this.email = email;
    }

    public String getLabel() {
        return label;
    }

    public String getEmail() {
        return email;
    }
}
