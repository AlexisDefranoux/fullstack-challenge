package com.alexis.defranoux.fullstack.challenge.backend.entities;

import com.alexis.defranoux.fullstack.challenge.backend.enums.QuestionType;
import com.alexis.defranoux.fullstack.challenge.backend.enums.TaxType;

import javax.persistence.*;
import java.io.File;

@Entity
public class ContactForm {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String email;

    private QuestionType questionType;

    private String tahitiNumber;

    private TaxType taxType;

    private String comment;

    @Lob
    private File file;

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public String getTahitiNumber() {
        return tahitiNumber;
    }

    public void setTahitiNumber(String tahitiNumber) {
        this.tahitiNumber = tahitiNumber;
    }

    public TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxType taxType) {
        this.taxType = taxType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
