package com.alexis.defranoux.fullstack.challenge.backend.exceptions;

public class EmailContactFormException extends ContactFormException {

    public EmailContactFormException(String email) {
        super("Invalid email format: " + email);
    }
}
