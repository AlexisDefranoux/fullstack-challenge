import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ContactForm} from "../models/contact-form";

@Injectable({
  providedIn: 'root'
})
export class ContactFromService {

  private readonly contactFormUrl: string;

  constructor(private http: HttpClient) {
    this.contactFormUrl = 'http://localhost:8080/contact-forms';
  }

  public postContactForm(contactForm: ContactForm): Observable<ContactForm> {
    return this.http.post<ContactForm>(this.contactFormUrl, contactForm);
  }

  public getContactForms(): Observable<ContactForm[]> {
    return this.http.get<ContactForm[]>(this.contactFormUrl);
  }

  public getContactForm(id: number): Observable<ContactForm> {
    return this.http.get<ContactForm>(this.contactFormUrl + "/" + id);
  }

  public deleteContactForms(): Observable<ContactForm[]> {
    return this.http.delete<ContactForm[]>(this.contactFormUrl);
  }

  public deleteContactForm(id: number): Observable<ContactForm> {
    return this.http.delete<ContactForm>(this.contactFormUrl + "/" + id);
  }

  public putContactForm(contactForm: ContactForm): Observable<ContactForm> {
    return this.http.put<ContactForm>(this.contactFormUrl + "/" + contactForm.id, contactForm);
  }

}
