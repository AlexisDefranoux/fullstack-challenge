import {Component} from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective} from "@angular/forms";
import {QUESTION_TYPES} from "../../constants/question-type.constant";
import {QuestionType} from "../../enums/question-type.enum";
import {ContactFromService} from "../../services/contact-from.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent {

  selectedQuestion?: string;

  dicpReceiptForm: FormGroup;
  litigationForm: FormGroup;
  statementForm: FormGroup;

  questionTypes = QUESTION_TYPES;
  questionType = QuestionType;

  constructor(
    private contactFormService: ContactFromService,
    private matSnackBar: MatSnackBar
  ) {
    this.dicpReceiptForm = new FormGroup({
      questionType: new FormControl()
    });

    this.litigationForm = new FormGroup({
      questionType: new FormControl(),
      comment: new FormControl()
    });

    this.statementForm = new FormGroup({
      questionType: new FormControl(),
      // file: new FormControl()
    });

    this.setQuestionTypeByForm();
  }

  public submit(formGroup: FormGroup, formGroupDirective: FormGroupDirective): void {
    this.contactFormService.postContactForm(formGroup.value).subscribe(
      () => {
        formGroupDirective.resetForm();
        this.setQuestionTypeByForm();
        this.selectedQuestion = undefined;
        this.matSnackBar.open('Votre demande a bien été transmise', 'X', {duration: 5000});
      },
      () => {
        this.matSnackBar.open('Une erreur a été rencontrée', 'X', {duration: 5000});
      },
    );
  }

  private setQuestionTypeByForm(): void {
    this.dicpReceiptForm.controls.questionType.setValue(QuestionType.DICP_RECEIPT);
    this.litigationForm.controls.questionType.setValue(QuestionType.LITIGATION);
    this.statementForm.controls.questionType.setValue(QuestionType.STATEMENT);
  }

}
