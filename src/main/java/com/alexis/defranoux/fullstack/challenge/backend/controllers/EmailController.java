package com.alexis.defranoux.fullstack.challenge.backend.controllers;

import com.alexis.defranoux.fullstack.challenge.backend.entities.ContactForm;
import com.alexis.defranoux.fullstack.challenge.backend.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class EmailController {

    private final EmailService emailService;

    @Autowired
    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/send")
    public ContactForm send(ContactForm contactForm) throws MessagingException {
        emailService.sendEmail(contactForm);
        return contactForm;
    }

}