export class ContactForm {
  id!: number;
  email!: string;
  questionType!: string;
  tahitiNumber!: string;
  taxType!: string;
  comment!: string;
  file!: File;
}
